package ru.abarabolenko.tests.pages;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.abarabolenko.pages.cellListPage.CellListPage;
import ru.abarabolenko.testingframework.CommonSettings;
import ru.abarabolenko.testingframework.entities.contact.Contact;
import ru.abarabolenko.testingframework.managers.contact.ContactCreator;
import ru.abarabolenko.testingframework.managers.contact.ContactManagerFacade;
import ru.abarabolenko.testingframework.testingtools.base.Utils;
import ru.abarabolenko.testingframework.testingtools.pageobjects.declaration.IWorkbench;
import ru.abarabolenko.testingframework.testingtools.pageobjects.implementation.Workbench;

import java.time.LocalDate;

public class CellListPageTest {

    private IWorkbench _workbench;

    @Before
    public void setUp() {
        _workbench = Workbench.getInstance();
    }

    @After
    public void tearDown() {
        CommonSettings.testCleanupAction(_workbench);
    }

    /**
     * Успешное создание контракта
     *
     * Описание: данный тестовый случай проверяет ожидаемое поведение функционала создания контакта на странице Cell List
     * и корректное отображение результата в списке всех контактов при передаче корректных параметров.
     *
     * Шаги:
     * - Переходим на страницу Cell List
     * - Заполняем поля в форме Contact Info корректными значениями
     * - Щелкаем кнопку Create Contact
     *
     * Ожидаемый результат:
     * - В списке Cell List отображается созданный контакт
     */
    @Test
    public void testCreateContact_CorrectValues_Success() {

        // ARRANGE

        _workbench.getTestingEngine().getBrowser().goToUrl(ru.abarabolenko.pages.cellListPage.CellListPage.url);

        ContactCreator creator = ContactCreator.generateCreator();
        creator.setCategory(Contact.Category.Friends);
        creator.setBirthday(LocalDate.of(Utils.getRandomBornYear(), Utils.getRandomMonth(), Utils.getRandomDay()));

        if (!_workbench.getTestingEngine().getElement().isElementPresent(CellListPage.UPDATE_CONTRACT_BUTTON_DISABLED_LOCATOR))
            throw new RuntimeException("Кнопка изменения контакта активна, если не выбран контакт");

        // ACT

        ContactManagerFacade contactManager = new ContactManagerFacade(_workbench);
        Contact contact = contactManager.createContact(creator);

        // ASSERT

        String pagingInfo = _workbench.getTestingEngine().getElement().getText(CellListPage.CELL_LIST_PAGING_INFO_LOCATOR);
        int contactId = Integer.parseInt(pagingInfo.split(" ")[4]);

        String contactInfoFromList = CellListPage.getContactInfoFromList(_workbench, contactId);

        checkContactInfo(contact, contactInfoFromList);
    }

    /**
     * Успешное обновление контакта
     *
     * Описание: данный тестовый случай проверяет ожидаемое поведение функционала изменения контакта на странице Cell List
     * и корректное отображение результата в списке всех контактов при передаче корректных параметров.
     *
     * Шаги:
     * - Переходим на страницу Cell List
     * - Создаём контакт
     * - Ищем созданный контакт в списке Cell List, щелкаем по нему
     * - В форме Contact Info заполняем поля корректными данными
     * - Щелкаем кнопку Update Contact
     *
     * Ожидаемый результат:
     * - В списке Cell List не создался новый контакт после обновления контакта
     * - В списке Cell List отображается обновлённый контакт
     */
    @Test
    public void testUpdateContact_CorrectValues_Success() {

        // ARRANGE

        _workbench.getTestingEngine().getBrowser().goToUrl(ru.abarabolenko.pages.cellListPage.CellListPage.url);

        ContactCreator oldCreator = ContactCreator.generateCreator();
        oldCreator.setCategory(Contact.Category.Friends);

        ContactManagerFacade contractManager = new ContactManagerFacade(_workbench);
        Contact contact = contractManager.createContact(oldCreator);

        String pagingInfoAfterCreate = _workbench.getTestingEngine().getElement().getText(CellListPage.CELL_LIST_PAGING_INFO_LOCATOR);
        int contactsCountAfterCreate = Integer.parseInt(pagingInfoAfterCreate.split(" ")[4]);

        // ACT

        ContactCreator newCreator = ContactCreator.generateCreator();
        newCreator.setCategory(Contact.Category.Coworkers);
        newCreator.setBirthday(LocalDate.of(Utils.getRandomBornYear(), Utils.getRandomMonth(), Utils.getRandomDay()));

        contractManager.updateContact(contact, newCreator);

        // ASSERT

        String pagingInfoAfterUpdate = _workbench.getTestingEngine().getElement().getText(CellListPage.CELL_LIST_PAGING_INFO_LOCATOR);
        int contactsCountAfterUpdate = Integer.parseInt(pagingInfoAfterUpdate.split(" ")[4]);

        Assert.assertEquals("System create another contact after Update", contactsCountAfterCreate, contactsCountAfterUpdate);

        int contactId = Integer.parseInt(pagingInfoAfterUpdate.split(" ")[4]);

        String contactInfoFromList = CellListPage.getContactInfoFromList(_workbench, contactId);
        checkContactInfo(contact, contactInfoFromList);
    }

    /**
     * Успешное создание контактов по кнопке Generate 50 Contacts
     *
     * Описание: данный тестовый случай проверяет ожидаемое поведение функционала генерации 50 контактов
     * на странице Cell List и корректное отображение результата в списке всех контактов.
     *
     * Шаги:
     * - Переходим на страницу Cell List
     * - Запоминаем количество контактов в списке Cell List
     * - Щелкаем кнопку Create 50 Contact
     *
     * Ожидаемый результат:
     * - В списке Cell List создались 50 новый контактов
     */
    @Test
    public void testCreate50Contacts_CorrectValues_Success() {

        // ARRANGE

        _workbench.getTestingEngine().getBrowser().goToUrl(ru.abarabolenko.pages.cellListPage.CellListPage.url);

        String pagingInfoAfterUpdate = _workbench.getTestingEngine().getElement().getText(CellListPage.CELL_LIST_PAGING_INFO_LOCATOR);
        int contactsCountBefore = Integer.parseInt(pagingInfoAfterUpdate.split(" ")[4]);

        // ACT

        ContactManagerFacade contractManager = new ContactManagerFacade(_workbench);
        contractManager.create50Contacts();

        // ASSERT

        pagingInfoAfterUpdate = _workbench.getTestingEngine().getElement().getText(CellListPage.CELL_LIST_PAGING_INFO_LOCATOR);
        int contactsCountAfter = Integer.parseInt(pagingInfoAfterUpdate.split(" ")[4]);

        Assert.assertEquals("There are created unexpected amount of contacts", contactsCountBefore + 50, contactsCountAfter);
    }

    private void checkContactInfo(Contact contact, String contactInfoFromList) {

        String[] contactInfoFromListSplitted = contactInfoFromList.split("\\n");

        String firstNameFromList = contactInfoFromListSplitted[0].split(" ")[0];
        String lastNameFromList = contactInfoFromListSplitted[0].split(" ")[1];
        String addressFromList = contactInfoFromListSplitted[1];

        Assert.assertEquals("Wrong first name displayed in List", contact.getFirstName(), firstNameFromList);
        Assert.assertEquals("Wrong last name displayed in List", contact.getLastName(), lastNameFromList);
        Assert.assertEquals("Wrong address displayed in List", contact.getAddress(), addressFromList);
    }
}
