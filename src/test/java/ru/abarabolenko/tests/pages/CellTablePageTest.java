package ru.abarabolenko.tests.pages;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import ru.abarabolenko.pages.celltablepage.CellTablePage;
import ru.abarabolenko.pages.celltablepage.CellTablePageRow;
import ru.abarabolenko.testingframework.CommonSettings;
import ru.abarabolenko.testingframework.entities.contact.Contact;
import ru.abarabolenko.testingframework.managers.contact.ContactCreator;
import ru.abarabolenko.testingframework.testingtools.base.Locator;
import ru.abarabolenko.testingframework.testingtools.base.LocatorType;
import ru.abarabolenko.testingframework.testingtools.base.Utils;
import ru.abarabolenko.testingframework.testingtools.pageobjects.declaration.IWorkbench;
import ru.abarabolenko.testingframework.testingtools.pageobjects.implementation.Workbench;

import java.util.ArrayList;
import java.util.List;

public class CellTablePageTest {
    private IWorkbench _workbench;

    private final Locator firstNameLocator = new Locator(LocatorType.Xpath, "//table[@class='GNHGC04CIE GNHGC04CKJ']/tbody[1]/tr[1]/td[2]/div");
    private final Locator lastNameLocator = new Locator(LocatorType.Xpath, "//table[@class='GNHGC04CIE GNHGC04CKJ']/tbody[1]/tr[1]/td[3]/div");
    private final Locator categoryLocator = new Locator(LocatorType.Xpath, "//table[@class='GNHGC04CIE GNHGC04CKJ']/tbody[1]/tr[1]/td[4]/div/select");

    @Before
    public void setUp() { _workbench = Workbench.getInstance(); }

    @After
    public void tearDown() {
        CommonSettings.testCleanupAction(_workbench);
    }

    /**
     * Успешное изменение данных в таблице Cell Table
     *
     * Описание: данный тестовый случай проверяет поведение таблицы контактов на странице Cell Table
     * при изменении значений корректных параметров.
     *
     * Шаги:
     * - Переходим на страницу Cell Table
     * - Меняем значения столбцов FirstName, LastName и Category у первого контакта в таблице Cell Table
     * - Щелкаем кнопку Next Page
     * - Щелкаем кнопку Previous Page
     *
     * Ожидаемый результат:
     * - В таблице Cell Table отображаются измененные данные контакта
     */
    @Test
    public void testChangeCellTableValues_CorrectValues_Success(){

        // ARRANGE

        _workbench.getTestingEngine().getBrowser().goToUrl(ru.abarabolenko.pages.celltablepage.CellTablePage.url);

        ContactCreator creator = ContactCreator.generateCreator();

        // ACT

        changeTableValue(_workbench, firstNameLocator, creator.getFirstName());

        changeTableValue(_workbench, lastNameLocator, creator.getLastName());

        _workbench.getComboBox().selectByName(categoryLocator, creator.getCategory().toString());

        _workbench.getButton().click(CellTablePage.NEXT_PAGE_BUTTON_LOCATOR);
        _workbench.getButton().click(CellTablePage.PREVIOUS_PAGE_BUTTON_LOCATOR);

        // ASSERT

        String actualFirstName = _workbench.getEditBox().getText(firstNameLocator);
        String actualLastName = _workbench.getEditBox().getText(lastNameLocator);
        String actualCategory = _workbench.getComboBox().getValue(categoryLocator);

        Assert.assertEquals("ErrFirstName", creator.getFirstName(), actualFirstName);
        Assert.assertEquals("ErrLastName", creator.getLastName(), actualLastName);
        Assert.assertEquals("ErrCategory", creator.getCategory().toString(), actualCategory);
    }

    /**
     * Проверка сортировки по столбцам в таблице Cell Table
     *
     * Описание: данный тестовый случай проверяет поведение таблицы контактов на странице Cell Table
     * при различной сортировке контактов.
     *
     * Шаги:
     * - Переходим на страницу Cell Table
     * - Щелкаем несколько раз в заголовке таблицы Cell Table по столбцу @ИмяСтолбца, запоминаем контакт, который отображается первым
     * - Щелкаем кнопку Last Page
     * - Щелкаем несколько раз в заголовке таблицы Cell Table по столбцу @ИмяСтолбца, запоминаем контакт, который отображается последним
     *
     * Ожидаемый результат:
     * - В таблице Cell Table корректно работает сортировка по столбцу
     * т.е. контакт, отображаемый первым на первой странице при изменении сортировки отображается последним на последней странице
     */
    @Test
    public void testCheckCellTableSort_SimpleCheck_Success(){

        // ARRANGE

        _workbench.getTestingEngine().getBrowser().goToUrl(ru.abarabolenko.pages.celltablepage.CellTablePage.url);

        List<Locator> columnLocatorList = new ArrayList<>();
        columnLocatorList.add(CellTablePage.FIRST_NAME_COLUMN_LOCATOR);
        columnLocatorList.add(CellTablePage.LAST_NAME_COLUMN_LOCATOR);
        columnLocatorList.add(CellTablePage.ADDRESS_COLUMN_LOCATOR);

        for(Locator columnLocator : columnLocatorList)
        {
            // ACT

            _workbench.getTestingEngine().getMouse().click(columnLocator);
            Contact firstContact = CellTablePageRow.getContactFromRow(_workbench, "1");

            _workbench.getTestingEngine().getMouse().click(columnLocator);
            Contact lastContact = CellTablePageRow.getContactFromRow(_workbench, "1");

            _workbench.getTestingEngine().getMouse().click(columnLocator);
            Contact expectedFirstContact = CellTablePageRow.getContactFromRow(_workbench, "1");

            // ASSERT

            assertEqualRows(expectedFirstContact, firstContact);

            // ACT

            _workbench.getTestingEngine().getMouse().click(columnLocator);
            Contact expectedLastContact = CellTablePageRow.getContactFromRow(_workbench, "1");

            // ASSERT

            assertEqualRows(expectedLastContact, lastContact);

            // ACT

            _workbench.getButton().click(CellTablePage.LAST_PAGE_BUTTON_LOCATOR);
            Contact expectedFirstContactLastPage = CellTablePageRow.getContactFromRow(_workbench, "last()");

            // ASSERT

            assertEqualRows(expectedFirstContactLastPage, firstContact);

            // ACT

            _workbench.getTestingEngine().getMouse().click(columnLocator);
            Contact expectedLastContactLastPage = CellTablePageRow.getContactFromRow(_workbench, "last()");

            // ASSERT

            assertEqualRows(expectedLastContactLastPage, lastContact);

            _workbench.getTestingEngine().getBrowser().refresh();
        }

    }

    /**
     * Проверка перехода по страницам таблицы Cell Table
     *
     * Описание: данный тестовый случай проверяет поведение таблицы контактов на странице Cell Table
     * при переходе между страницами.
     *
     * Шаги:
     * - Переходим на страницу Cell Table
     * - Проверяем, что на первой странице неактивны кнопки Previous Page и First Page
     * - Доходим до последней страницы: последовательно щелкаем кнопку Next Page и проверяем, что все 4 кнопки перехода активны
     * - Проверяем, что на последней странице неактивны кнопки Next Page и Last Page
     * - Доходим до первой страницы: последовательно щелкаем кнопку Previous Page и проверяем, что все 4 кнопки перехода активны
     * - Проверяем, что на первой странице остались неактивны кнопки Previous Page и First Page
     *
     * Ожидаемый результат:
     * - В таблице Cell Table корректно работает paging (переход между страницами таблицы)
     */
    @Test
    public void testCheckCellTablePaging_SimpleCheck_Success(){

        // ARRANGE

        _workbench.getTestingEngine().getBrowser().goToUrl(ru.abarabolenko.pages.celltablepage.CellTablePage.url);

        // ASSERT

        _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.FIRST_PAGE_BUTTON_DISABLED_LOCATOR);
        _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.PREVIOUS_PAGE_BUTTON_DISABLED_LOCATOR);
        _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.NEXT_PAGE_BUTTON_LOCATOR);
        _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.LAST_PAGE_BUTTON_LOCATOR);

        // ACT

        _workbench.getButton().click(CellTablePage.NEXT_PAGE_BUTTON_LOCATOR);

        for (int i = 0; i < 15; i++) {

            // ASSERT

            _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.FIRST_PAGE_BUTTON_LOCATOR);
            _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.PREVIOUS_PAGE_BUTTON_LOCATOR);
            _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.NEXT_PAGE_BUTTON_LOCATOR);
            _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.LAST_NAME_COLUMN_LOCATOR);

            // ACT

            _workbench.getButton().click(CellTablePage.NEXT_PAGE_BUTTON_LOCATOR);
        }

        // ASSERT

        _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.FIRST_PAGE_BUTTON_LOCATOR);
        _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.PREVIOUS_PAGE_BUTTON_LOCATOR);
        _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.NEXT_PAGE_BUTTON_DISABLED_LOCATOR);
        _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.LAST_PAGE_BUTTON_DISABLED_LOCATOR);

        // ACT

        _workbench.getButton().click(CellTablePage.PREVIOUS_PAGE_BUTTON_LOCATOR);

        for (int i = 0; i < 15; i++) {

            // ASSERT

            _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.FIRST_PAGE_BUTTON_LOCATOR);
            _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.PREVIOUS_PAGE_BUTTON_LOCATOR);
            _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.NEXT_PAGE_BUTTON_LOCATOR);
            _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.LAST_PAGE_BUTTON_LOCATOR);

            // ACT

            _workbench.getButton().click(CellTablePage.PREVIOUS_PAGE_BUTTON_LOCATOR);
        }

        // ASSERT

        _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.FIRST_PAGE_BUTTON_DISABLED_LOCATOR);
        _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.PREVIOUS_PAGE_BUTTON_DISABLED_LOCATOR);
        _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.NEXT_PAGE_BUTTON_LOCATOR);
        _workbench.getTestingEngine().getElement().isElementPresent(CellTablePage.LAST_PAGE_BUTTON_LOCATOR);
    }

    private static void assertEqualRows(Contact first, Contact second) {

        Assert.assertEquals("ErrFirstName", first.getFirstName(), second.getFirstName());
        Assert.assertEquals("ErrLastName", first.getLastName(), second.getLastName());
        Assert.assertEquals("ErrCategory", first.getCategory(), second.getCategory());
        Assert.assertEquals("ErrAddress", first.getAddress(), second.getAddress());
    }

    private void changeTableValue(IWorkbench workbench, Locator locator, String newValue) {

        WebDriver driver = workbench.getTestingEngine().getWebDriver();
        WebElement element = Utils.getElement(driver, locator);
        Actions action = new Actions(driver);
        action.click(element).sendKeys(newValue).sendKeys(Keys.ENTER).build().perform();
    }
}
