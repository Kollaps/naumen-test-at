package ru.abarabolenko.testingframework.entities.contact;

import ru.abarabolenko.testingframework.managers.contact.ContactCreator;

import java.time.LocalDate;

public class Contact {

    private String firstName;
    private String lastName;
    private Category category;
    private LocalDate birthday;
    private String address;

    private Contact() {
        // Make this class unable to create. Because this is a simple dictionary.
    }

    public static Contact getContact(ContactCreator creator) {

        Contact contact = new Contact();
        contact.firstName = creator.getFirstName();
        contact.lastName = creator.getLastName();
        contact.category = creator.getCategory();
        contact.birthday = creator.getBirthday();
        contact.address = creator.getAddress();

        return contact;
    }

    public static Contact getEmptyContact() {

        Contact contact = new Contact();

        return contact;
    }

    public static void updateContact(Contact contact, ContactCreator creator) {

        contact.firstName = creator.getFirstName();
        contact.lastName = creator.getLastName();
        contact.category = creator.getCategory();
        contact.birthday = creator.getBirthday();
        contact.address = creator.getAddress();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Category getCategory() {
        return category;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public String getAddress() {
        return address;
    }

    public enum Category {
        Family,
        Friends,
        Coworkers,
        Businesses,
        Category,
        Contacts,
    }

}
