package ru.abarabolenko.testingframework;


import ru.abarabolenko.testingframework.testingtools.pageobjects.declaration.IWorkbench;

public class CommonSettings {
    public static void testCleanupAction(IWorkbench workbench)
    {
        if (workbench != null)
            workbench.close();
    }
}
