package ru.abarabolenko.testingframework.managers.contact;

import ru.abarabolenko.testingframework.entities.contact.Contact;
import ru.abarabolenko.testingframework.testingtools.base.Utils;

import java.time.LocalDate;
import java.util.Random;

public class ContactCreator {

    private String firstName;
    private String lastName;
    private Contact.Category category;
    private LocalDate birthday;
    private String address;

    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }

    public Contact.Category getCategory() { return category; }
    public void setCategory(Contact.Category category) { this.category = category; }

    public LocalDate getBirthday() { return birthday; }
    public void setBirthday(LocalDate birthday) { this.birthday = birthday; }

    public String getAddress() { return address; }
    public void setAddress(String address) { this.address = address; }

    public static ContactCreator generateCreator() {

        Random r = new Random();

        ContactCreator contactCreator =  new ContactCreator();

        contactCreator.firstName = Utils.getRandomAlphabetic(12);
        contactCreator.lastName = Utils.getRandomAlphabetic(12);
        contactCreator.category = Contact.Category.Businesses;
        contactCreator.birthday = LocalDate.of(Utils.getRandomBornYear(), Utils.getRandomMonth(), Utils.getRandomDay());
        contactCreator.address = Utils.getRandomAlphabetic(12);

        return contactCreator;
    }
}
