package ru.abarabolenko.testingframework.managers.contact;

import org.openqa.selenium.Keys;
import ru.abarabolenko.pages.cellListPage.CellListPage;
import ru.abarabolenko.testingframework.entities.contact.Contact;
import ru.abarabolenko.testingframework.testingtools.pageobjects.declaration.IWorkbench;

import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class ContactManagerFacade {

    private IWorkbench _workbench;

    public ContactManagerFacade(IWorkbench workbench) {
        _workbench = workbench;
    }

    public Contact createContact(ContactCreator creator) {

        _workbench.getEditBox().type(CellListPage.FIRST_NAME_LOCATOR, creator.getFirstName());
        _workbench.getEditBox().type(CellListPage.LAST_NAME_LOCATOR, creator.getLastName());
        _workbench.getComboBox().selectByName(CellListPage.CATEGORY_LOCATOR, creator.getCategory().toString());

        _workbench.getEditBox().type(CellListPage.BIRTHDAY_LOCATOR, creator.getBirthday().format(DateTimeFormatter.ofPattern("MMMM dd, yyyy", Locale.US)));
        _workbench.getTestingEngine().getKeyboard().keyPress(CellListPage.BIRTHDAY_LOCATOR, Keys.ENTER.toString());

        _workbench.getEditBox().type(CellListPage.ADDRESS_LOCATOR, creator.getAddress());

        _workbench.getButton().click(CellListPage.CREATE_CONTRACT_BUTTON_LOCATOR);

        return Contact.getContact(creator);
    }

    public void updateContact(Contact oldContact, ContactCreator creator) {

        _workbench.getEditBox().type(CellListPage.FIRST_NAME_LOCATOR, creator.getFirstName());
        _workbench.getEditBox().type(CellListPage.LAST_NAME_LOCATOR, creator.getLastName());
        _workbench.getComboBox().selectByName(CellListPage.CATEGORY_LOCATOR, creator.getCategory().toString());
        _workbench.getEditBox().type(CellListPage.BIRTHDAY_LOCATOR, creator.getBirthday().format(DateTimeFormatter.ofPattern("MMMM dd, yyyy", Locale.US)));
        _workbench.getTestingEngine().getKeyboard().keyPress(CellListPage.BIRTHDAY_LOCATOR, Keys.ENTER.toString());
        _workbench.getEditBox().type(CellListPage.ADDRESS_LOCATOR, creator.getAddress());

        _workbench.getButton().click(CellListPage.UPDATE_CONTRACT_BUTTON_LOCATOR);

        Contact.updateContact(oldContact, creator);
    }

    public void create50Contacts() {

        _workbench.getButton().click(CellListPage.CREATE_50_CONTACTS_BUTTON_LOCATOR);
    }
}
