package ru.abarabolenko.testingframework.testingtools.pageobjects.declaration;

import ru.abarabolenko.testingframework.testingtools.base.Locator;

public interface IEditBox {

    void type(Locator editBoxLocator, String value);
    String getText(Locator editBoxLocator);
}
