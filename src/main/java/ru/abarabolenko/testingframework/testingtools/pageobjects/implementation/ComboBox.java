package ru.abarabolenko.testingframework.testingtools.pageobjects.implementation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import ru.abarabolenko.testingframework.testingtools.base.Utils;
import ru.abarabolenko.testingframework.testingtools.base.Locator;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IMultiToolWebTestingEngine;
import ru.abarabolenko.testingframework.testingtools.pageobjects.declaration.IComboBox;

public class ComboBox implements IComboBox {

    private final WebDriver _webDriver;
    private final IMultiToolWebTestingEngine _engine;

    public ComboBox(IMultiToolWebTestingEngine engine)
    {
        _webDriver = engine.getWebDriver();
        _engine = engine;
    }

    public void selectByIndex(Locator comboBoxLocator, int index) {

        Select comboBox = new Select(Utils.getElement(_webDriver, comboBoxLocator));

        comboBox.selectByIndex(index);
    }

    public void selectByName(Locator comboBoxLocator, String name) {

        Select comboBox = new Select(Utils.getElement(_webDriver, comboBoxLocator));

        comboBox.selectByVisibleText(name);
    }

    public String getValue(Locator comboBoxLocator) {

        return _engine.getElement().getText(new Locator(comboBoxLocator.locatorType, comboBoxLocator.locatorValue + "/option[@selected='selected']"));
    }
}
