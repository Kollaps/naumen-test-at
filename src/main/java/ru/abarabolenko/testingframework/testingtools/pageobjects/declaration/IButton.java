package ru.abarabolenko.testingframework.testingtools.pageobjects.declaration;

import ru.abarabolenko.testingframework.testingtools.base.Locator;

public interface IButton {

    void click(Locator locator);
}
