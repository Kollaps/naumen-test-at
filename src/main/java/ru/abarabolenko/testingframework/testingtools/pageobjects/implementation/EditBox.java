package ru.abarabolenko.testingframework.testingtools.pageobjects.implementation;

import ru.abarabolenko.testingframework.testingtools.base.Locator;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IMultiToolWebTestingEngine;
import ru.abarabolenko.testingframework.testingtools.pageobjects.declaration.IEditBox;

public class EditBox implements IEditBox {

    private final IMultiToolWebTestingEngine _engine;

    public EditBox(IMultiToolWebTestingEngine engine)
    {
        _engine = engine;
    }

    public void type(Locator editBoxLocator, String value)
    {
        _engine.getKeyboard().clearAndKeyPress(editBoxLocator, value);
    }

    public String getText(Locator editBoxLocator)
    {
        return _engine.getElement().getText(editBoxLocator);
    }
}
