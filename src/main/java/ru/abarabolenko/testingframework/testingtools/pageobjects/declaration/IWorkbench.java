package ru.abarabolenko.testingframework.testingtools.pageobjects.declaration;

import ru.abarabolenko.testingframework.testingtools.base.declaration.IMultiToolWebTestingEngine;

public interface IWorkbench {

    void close();
    IComboBox getComboBox();
    IEditBox getEditBox();
    IButton getButton();
    IMultiToolWebTestingEngine getTestingEngine();
}
