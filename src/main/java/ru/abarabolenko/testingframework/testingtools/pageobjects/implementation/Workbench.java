package ru.abarabolenko.testingframework.testingtools.pageobjects.implementation;

import com.sun.media.jfxmediaimpl.MediaDisposer;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import ru.abarabolenko.testingframework.testingtools.base.BrowserType;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IMultiToolWebTestingEngine;
import ru.abarabolenko.testingframework.testingtools.base.implementation.MultiToolWebTestingEngine;
import ru.abarabolenko.testingframework.testingtools.base.WebDriverSettings;
import ru.abarabolenko.testingframework.testingtools.pageobjects.declaration.IButton;
import ru.abarabolenko.testingframework.testingtools.pageobjects.declaration.IComboBox;
import ru.abarabolenko.testingframework.testingtools.pageobjects.declaration.IEditBox;
import ru.abarabolenko.testingframework.testingtools.pageobjects.declaration.IWorkbench;

import javax.swing.*;
import java.time.Duration;

public class Workbench implements IWorkbench, MediaDisposer.Disposable {

    private IMultiToolWebTestingEngine _multiToolWebTestingEngine;
    private IComboBox _comboBox;
    private IButton _button;
    private IEditBox _editBox;
    public Workbench(WebDriverSettings webDriverSettings, String url, Duration implicitlyWait, Duration pageToLoadWait) {

        initialize(webDriverSettings, url, implicitlyWait, pageToLoadWait);
    }

    private void initialize(WebDriverSettings webDriverSettings, String url, Duration implicitlyWait, Duration pageToLoadWait) {

        try
        {
            _multiToolWebTestingEngine = new MultiToolWebTestingEngine(url, webDriverSettings, implicitlyWait, pageToLoadWait);
        }
        catch (WebDriverException ex)
        {
            throw new RuntimeException(String.format("��� ������������� webDriver ��������� ������:\n{0}", ex.getMessage()));
        }

        _button = new Button(_multiToolWebTestingEngine);
        _comboBox = new ComboBox(_multiToolWebTestingEngine);
        _editBox = new EditBox(_multiToolWebTestingEngine);
    }

    public IMultiToolWebTestingEngine getTestingEngine() { return _multiToolWebTestingEngine; }

    public IComboBox getComboBox() { return _comboBox; }

    public IEditBox getEditBox() { return _editBox; }

    public IButton getButton() { return _button; }

    public void close()
    {
        if (_multiToolWebTestingEngine == null) return;
        _multiToolWebTestingEngine.close();
        _isDisposed = true;
    }

    private boolean _isDisposed;

    public void dispose() {
        if (!_isDisposed)
            close();
    }

    public static IWorkbench getInstance() {
        WebDriverSettings settings = new WebDriverSettings();

        //settings.profilePath = "C:\\SeleniumProfile";
        settings.browser = BrowserType.FireFox;

        return new Workbench(settings, "localhost:80", Duration.ofSeconds(60), Duration.ofSeconds(60));
    }
}
