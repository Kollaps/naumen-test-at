package ru.abarabolenko.testingframework.testingtools.pageobjects.implementation;

import ru.abarabolenko.testingframework.testingtools.base.Locator;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IMultiToolWebTestingEngine;
import ru.abarabolenko.testingframework.testingtools.pageobjects.declaration.IButton;

public class Button implements IButton {

    private final IMultiToolWebTestingEngine _engine;

    public Button(IMultiToolWebTestingEngine engine)
    {
        _engine = engine;
    }

    public void click(Locator locator)
    {
        _engine.getMouse().click(locator);
    }
}
