package ru.abarabolenko.testingframework.testingtools.pageobjects.declaration;

import ru.abarabolenko.testingframework.testingtools.base.Locator;

public interface IComboBox {

    void selectByIndex(Locator comboBoxLocator, int index);

    void selectByName(Locator comboBoxLocator, String name);

    String getValue(Locator comboBoxLocator);
}
