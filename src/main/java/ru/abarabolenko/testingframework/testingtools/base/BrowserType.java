package ru.abarabolenko.testingframework.testingtools.base;

public enum BrowserType {
    InternetExplorer,
    FireFox,
    Chrome
}
