package ru.abarabolenko.testingframework.testingtools.base;

public class WebDriverSettings {

    public BrowserType browser;
    public String profilePath;

    public BrowserType getBrowser() { return browser; }
}
