package ru.abarabolenko.testingframework.testingtools.base.declaration;

import ru.abarabolenko.testingframework.testingtools.base.Locator;

public interface IElement {

    boolean isElementPresent(Locator locator);

    String getText(Locator locator);

    String getAttribute(Locator locator, String attribute);
}