package ru.abarabolenko.testingframework.testingtools.base;

public enum LocatorType {
    Id,
    Name,
    Link,
    Css,
    Xpath
}
