package ru.abarabolenko.testingframework.testingtools.base.declaration;

import ru.abarabolenko.testingframework.testingtools.base.Locator;

import java.time.Duration;

public interface IWaiting {

    void waitForReadyStateComplete();
    Duration getImplicitlyWait();
    Duration getPageToLoadWait();
}