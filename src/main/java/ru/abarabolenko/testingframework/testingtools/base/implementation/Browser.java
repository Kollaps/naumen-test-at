package ru.abarabolenko.testingframework.testingtools.base.implementation;

import org.openqa.selenium.WebDriver;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IMultiToolWebTestingEngine;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IBrowser;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IWaiting;

public class Browser implements IBrowser {

    private final WebDriver _webDriver;
    private final IWaiting _waiting;
    private final IMultiToolWebTestingEngine _engine;

    public Browser(WebDriver webDriver, IWaiting waiting, IMultiToolWebTestingEngine engine)
    {
        _webDriver = webDriver;
        _waiting = waiting;
        _engine = engine;
    }

    public void refresh() {
        _webDriver.navigate().refresh();
    }

    public void goToUrl(String url) {
        _webDriver.navigate().to(url);
    }

    public String  getPageTitle() {

        return _webDriver.getTitle();
    }

    public String getHtmlSource() {
        _waiting.waitForReadyStateComplete();

        return _webDriver.getPageSource();
    }
}
