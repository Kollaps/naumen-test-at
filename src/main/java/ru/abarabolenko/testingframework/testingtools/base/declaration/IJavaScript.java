package ru.abarabolenko.testingframework.testingtools.base.declaration;

public interface IJavaScript {

    String executeScalar(String javaScript);

    void executeScript(String javaScript);
}
