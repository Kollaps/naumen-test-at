package ru.abarabolenko.testingframework.testingtools.base.implementation;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import ru.abarabolenko.testingframework.testingtools.base.Utils;
import ru.abarabolenko.testingframework.testingtools.base.Locator;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IKeyboard;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IMultiToolWebTestingEngine;

public class Keyboard implements IKeyboard {

    private final WebDriver _webDriver;
    private final IMultiToolWebTestingEngine _engine;

    public Keyboard(WebDriver webDriver, IMultiToolWebTestingEngine engine) {
        _webDriver = webDriver;
        _engine = engine;
    }

    public void keyPress(Locator locator, String keysSequence) {
        WebElement element = Utils.getElement(_webDriver, locator);
        element.sendKeys(keysSequence);
    }

    public void clearAndKeyPress(Locator locator, String keysSequence) {
        WebElement element = Utils.getElement(_webDriver, locator);
        element.clear();
        element.sendKeys(keysSequence);
    }

    public void keyDown(Locator locator, Keys key) {
        WebElement element = Utils.getElement(_webDriver, locator);
        Actions builder = new Actions(_webDriver);

        builder.keyDown(element, key).build().perform();
    }

    public void keyUp(Locator locator, Keys key) {
        WebElement element = Utils.getElement(_webDriver, locator);
        Actions builder = new Actions(_webDriver);
        builder.keyUp(element, key).build().perform();
    }
}
