package ru.abarabolenko.testingframework.testingtools.base.implementation;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IMultiToolWebTestingEngine;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IWaiting;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class Waiting implements IWaiting {
    private final WebDriver _webDriver;
    private final IMultiToolWebTestingEngine _engine;
    private Duration _implicitlyWait;
    private Duration _pageToLoadWait;

    public Waiting(WebDriver webDriver, Duration implicitlyWait, Duration pageToLoadWait, IMultiToolWebTestingEngine multiToolEngine)
    {
        _webDriver = webDriver;
        _implicitlyWait = implicitlyWait;
        _pageToLoadWait = pageToLoadWait;
        _engine = multiToolEngine;
    }

    public void waitForReadyStateComplete()
    {
        Wait<WebDriver> wait = new WebDriverWait(_webDriver, _pageToLoadWait.getSeconds());
        wait.until(driver -> ((JavascriptExecutor)_webDriver).executeScript("document.readyState").toString().toLowerCase() == "complete");
    }

    public Duration getImplicitlyWait() { return _implicitlyWait; }

    public void setImplicitlyWait(Duration implicitlyWait) {
        _webDriver.manage().timeouts().implicitlyWait(_implicitlyWait.getSeconds(), TimeUnit.SECONDS);
        _webDriver.manage().timeouts().setScriptTimeout(_implicitlyWait.getSeconds(), TimeUnit.SECONDS);

        _implicitlyWait = implicitlyWait;
    }

    public Duration getPageToLoadWait() { return _pageToLoadWait; }

    public void setPageToLoadWait(Duration pageToLoadWait) {
        _webDriver.manage().timeouts().pageLoadTimeout(pageToLoadWait.getSeconds(), TimeUnit.SECONDS);
        _pageToLoadWait = pageToLoadWait;
    }

}
