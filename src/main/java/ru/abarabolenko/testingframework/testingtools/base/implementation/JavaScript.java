package ru.abarabolenko.testingframework.testingtools.base.implementation;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import ru.abarabolenko.testingframework.testingtools.base.WebDriverSettings;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IJavaScript;

import javax.jws.WebResult;
import java.time.Duration;

public class JavaScript implements IJavaScript {

    private final WebDriver _webDriver;

    public JavaScript(WebDriver webDriver)
    {
        _webDriver = webDriver;
    }

    public String executeScalar(String javaScript)
    {
        return ((JavascriptExecutor)_webDriver).executeScript(javaScript).toString();
    }

    public void executeScript(String javaScript)
    {
        ((JavascriptExecutor)_webDriver).executeScript(javaScript);
    }
}
