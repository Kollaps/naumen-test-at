package ru.abarabolenko.testingframework.testingtools.base;

public class Locator {
    public LocatorType locatorType;
    public String locatorValue;

    public Locator(LocatorType type, String value) {
        locatorType = type;
        locatorValue = value;
    }
}