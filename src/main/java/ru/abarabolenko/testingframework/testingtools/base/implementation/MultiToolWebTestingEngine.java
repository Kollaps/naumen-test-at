package ru.abarabolenko.testingframework.testingtools.base.implementation;

import com.sun.media.jfxmediaimpl.MediaDisposer;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import ru.abarabolenko.testingframework.testingtools.base.WebDriverSettings;
import ru.abarabolenko.testingframework.testingtools.base.declaration.*;

import java.io.File;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class MultiToolWebTestingEngine implements IMultiToolWebTestingEngine, MediaDisposer.Disposable {

    private WebDriver _webDriver;
    private IKeyboard _keyBoard;
    private IMouse _mouse;
    private IElement _element;
    private IWaiting _waiting;
    private IBrowser _browser;
    private IJavaScript _javaScript;

    private void PrepareWebDriver(String browserUrl, Duration implicitlyWait, Duration pageToLoadWait) {
        
        _webDriver.manage().timeouts().implicitlyWait(implicitlyWait.getSeconds(), TimeUnit.SECONDS);
        _webDriver.manage().timeouts().pageLoadTimeout(pageToLoadWait.getSeconds(), TimeUnit.SECONDS);
        _webDriver.manage().timeouts().setScriptTimeout(implicitlyWait.getSeconds(), TimeUnit.SECONDS);

        _webDriver.navigate().to(browserUrl);

        _waiting = new Waiting(_webDriver, implicitlyWait, pageToLoadWait, this);
        _keyBoard = new Keyboard(_webDriver, this);
        _mouse = new Mouse(_webDriver, this);
        _browser = new Browser(_webDriver, _waiting, this);
        _element = new Element(_webDriver, _waiting, _browser, this);
        _javaScript = new JavaScript(_webDriver);
    }

    public MultiToolWebTestingEngine(String url, WebDriverSettings settings, Duration implicitlyWait, Duration pageToLoadWait) {

        switch (settings.getBrowser()) {
            case InternetExplorer:
                _webDriver = new InternetExplorerDriver();
                break;
            case FireFox:
                _webDriver = new FirefoxDriver();
                break;
            case Chrome:
            {
                ChromeOptions options = new ChromeOptions();
                _webDriver = new ChromeDriver(options);}
            break;
            default:
                throw new RuntimeException("not implemented browserType");
        }

        PrepareWebDriver(url, implicitlyWait, pageToLoadWait);
    }

    public IKeyboard getKeyboard() { return _keyBoard; }

    public IMouse getMouse() { return _mouse; }

    public WebDriver getWebDriver() { return _webDriver; }

    public IWaiting getWaiting() { return _waiting; }

    public IBrowser getBrowser() { return _browser; }

    public IElement getElement() { return _element; }

    public IJavaScript getJavaScript() { return _javaScript; }

    public void close() {
        _webDriver.quit();
        _isDisposed = true;
    }

    private boolean _isDisposed;

    public void dispose() {
        if (!_isDisposed)
            close();
    }
}
