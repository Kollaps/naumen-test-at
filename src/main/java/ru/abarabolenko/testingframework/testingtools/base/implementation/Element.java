package ru.abarabolenko.testingframework.testingtools.base.implementation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import ru.abarabolenko.testingframework.testingtools.base.Utils;
import ru.abarabolenko.testingframework.testingtools.base.Locator;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IBrowser;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IElement;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IMultiToolWebTestingEngine;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IWaiting;

public class Element implements IElement {

    private final WebDriver _webDriver;
    private final IWaiting _waiting;
    private final IBrowser _browser;
    private final IMultiToolWebTestingEngine _engine;

    public Element(WebDriver webDriver, IWaiting waiting, IBrowser browser, IMultiToolWebTestingEngine engine)
    {
        _webDriver = webDriver;
        _waiting = waiting;
        _browser = browser;
        _engine = engine;
    }

    public void clear(Locator locator)
    {
        Utils.getElement(_webDriver, locator).clear();
    }

    public void selectByName(Locator locator, String value)
    {
        Select element = new Select(Utils.getElement(_webDriver, locator));
        element.selectByVisibleText(value);
    }

    public void selectByIndex(Locator locator, int index)
    {
        Select element = new Select(Utils.getElement(_webDriver, locator));

        element.selectByIndex(index);
    }

    public boolean isElementPresent(Locator locator) {

        switch (locator.locatorType) {
            case Id:
                return _webDriver.findElements(By.id(locator.locatorValue)).size() != 0;
            case Name:
                return _webDriver.findElements(By.name(locator.locatorValue)).size() != 0;
            case Link:
                return _webDriver.findElements(By.linkText(locator.locatorValue)).size() != 0;
            case Css:{
                return _webDriver.findElements(By.cssSelector(locator.locatorValue)).size() != 0;
            }
            case Xpath:
                return _webDriver.findElements(By.xpath(locator.locatorValue)).size() != 0;
            default:
                throw new RuntimeException("����� �� �������������� ��� ��������");
        }
    }

    public String getText(Locator locator)
    {
        return Utils.getElement(_webDriver, locator).getText();
    }

    public String getAttribute(Locator locator, String attribute)
    {
        return Utils.getElement(_webDriver, locator).getAttribute(attribute);
    }


}
