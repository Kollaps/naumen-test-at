package ru.abarabolenko.testingframework.testingtools.base.declaration;

public interface IBrowser {
    void refresh();
    void goToUrl(String url);
    String getPageTitle();
    String getHtmlSource();
}
