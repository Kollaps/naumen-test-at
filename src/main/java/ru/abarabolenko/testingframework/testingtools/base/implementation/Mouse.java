package ru.abarabolenko.testingframework.testingtools.base.implementation;

import com.steadystate.css.parser.Locatable;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.internal.Coordinates;
import ru.abarabolenko.testingframework.testingtools.base.Utils;
import ru.abarabolenko.testingframework.testingtools.base.Locator;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IMouse;
import ru.abarabolenko.testingframework.testingtools.base.declaration.IMultiToolWebTestingEngine;

public class Mouse implements IMouse {

    private final WebDriver _webDriver;
    private final IMultiToolWebTestingEngine _engine;

    public Mouse(WebDriver webDriver, IMultiToolWebTestingEngine engine)
    {
        _webDriver= webDriver;
        _engine = engine;
    }

    public void click(Locator locator) {
        WebElement element = Utils.getElement(_webDriver, locator);
        Actions action = new Actions(_webDriver);

        action.click(element).perform();
    }

    public void doubleClick(Locator locator) {
        WebElement element = Utils.getElement(_webDriver, locator);
        Actions action = new Actions(_webDriver);

        action.doubleClick(element).perform();
    }

    public void over(Locator locator) {
        WebElement element = Utils.getElement(_webDriver, locator);
        Actions action = new Actions(_webDriver);

        action.moveToElement(element, 0, 0).build().perform();
    }
    public void up(Locator locator) {
        Locatable hoverItem = (Locatable) Utils.getElement(_webDriver, locator);

        org.openqa.selenium.interactions.Mouse mouse = ((HasInputDevices) _webDriver).getMouse();
        mouse.mouseUp((Coordinates)hoverItem);
    }
    public void down(Locator locator) {
        Locatable hoverItem = (Locatable) Utils.getElement(_webDriver, locator);

        org.openqa.selenium.interactions.Mouse mouse = ((HasInputDevices) _webDriver).getMouse();
        mouse.mouseDown((Coordinates) hoverItem);
    }

    public void moveByOffset(int x, int y)
    {
        Actions action = new Actions(_webDriver);

        action.moveByOffset(x, y).build().perform();
    }
}
