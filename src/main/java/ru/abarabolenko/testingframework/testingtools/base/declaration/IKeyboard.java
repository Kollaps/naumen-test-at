package ru.abarabolenko.testingframework.testingtools.base.declaration;

import org.openqa.selenium.Keys;
import ru.abarabolenko.testingframework.testingtools.base.Locator;

public interface IKeyboard {

    void keyPress(Locator locator, String keysSequence);

    void clearAndKeyPress(Locator locator, String keysSequence);

    void keyDown(Locator locator, Keys keysSequence);

    void keyUp(Locator locator, Keys keysSequence);
}
