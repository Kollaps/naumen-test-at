package ru.abarabolenko.testingframework.testingtools.base;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Random;

public class Utils {

    public static WebElement getElement(WebDriver webDriver, Locator locator) {
        switch (locator.locatorType) {
            case Id:
                return webDriver.findElement(By.id(locator.locatorValue));
            case Name:
                return webDriver.findElement(By.name(locator.locatorValue));
            case Link:
                return webDriver.findElement(By.linkText(locator.locatorValue));
            case Css:{
                return webDriver.findElement(By.cssSelector(locator.locatorValue));
            }
            case Xpath:
                return webDriver.findElement(By.xpath(locator.locatorValue));
            default:
                throw new RuntimeException("����� ���������������� ��� ��������");
        }
    }

    public static String getRandomAlphabetic(int n) {

        return RandomStringUtils.randomAlphabetic(n);
    }

    public static int getRandomDay() {
        return RandomUtils.nextInt(1, 29);
    }

    public static int getRandomMonth() {
        return RandomUtils.nextInt(1, 13);
    }

    public static int getRandomBornYear() {
        return RandomUtils.nextInt(1950, 2010);
    }
}
