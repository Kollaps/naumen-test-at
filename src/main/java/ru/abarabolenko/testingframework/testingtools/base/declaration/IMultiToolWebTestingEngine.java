package ru.abarabolenko.testingframework.testingtools.base.declaration;

import org.openqa.selenium.WebDriver;

public interface IMultiToolWebTestingEngine {

    void close();
    IKeyboard getKeyboard();
    IMouse getMouse();
    IBrowser getBrowser();
    IJavaScript getJavaScript();
    IElement getElement();
    IWaiting getWaiting();
    WebDriver getWebDriver();
}
