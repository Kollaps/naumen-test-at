package ru.abarabolenko.testingframework.testingtools.base.declaration;

import ru.abarabolenko.testingframework.testingtools.base.Locator;

public interface IMouse {

    void click(Locator locator);

    void doubleClick(Locator locator);

    void over(Locator locator);

    void down(Locator locator);

    void up(Locator locator);

    void moveByOffset(int x, int y);
}
