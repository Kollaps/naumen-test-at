package ru.abarabolenko.pages.celltablepage;

import ru.abarabolenko.testingframework.testingtools.base.Locator;
import ru.abarabolenko.testingframework.testingtools.base.LocatorType;

import java.util.HashMap;
import java.util.Map;

public class CellTablePage {

    public static final String url = "http://samples.gwtproject.org/samples/Showcase/Showcase.html#!CwCellTable";

    public static final Locator FIRST_NAME_COLUMN_LOCATOR = new Locator(LocatorType.Xpath, "//table[@class='GNHGC04CIE GNHGC04CKJ']/thead/tr/th[2]");
    public static final Locator LAST_NAME_COLUMN_LOCATOR = new Locator(LocatorType.Xpath, "//table[@class='GNHGC04CIE GNHGC04CKJ']/thead/tr/th[3]");
    public static final Locator ADDRESS_COLUMN_LOCATOR = new Locator(LocatorType.Xpath, "//table[@class='GNHGC04CIE GNHGC04CKJ']/thead/tr/th[5]");

    public static final Locator FIRST_PAGE_BUTTON_LOCATOR = new Locator(LocatorType.Xpath, "//img[@aria-label='First page'][@aria-disabled='false']");
    public static final Locator PREVIOUS_PAGE_BUTTON_LOCATOR = new Locator(LocatorType.Xpath, "//img[@aria-label='Previous page'][@aria-disabled='false']");
    public static final Locator NEXT_PAGE_BUTTON_LOCATOR = new Locator(LocatorType.Xpath, "//img[@aria-label='Next page'][@aria-disabled='false']");
    public static final Locator LAST_PAGE_BUTTON_LOCATOR = new Locator(LocatorType.Xpath, "//img[@aria-label='Last page'][@aria-disabled='false']");

    public static final Locator FIRST_PAGE_BUTTON_DISABLED_LOCATOR = new Locator(LocatorType.Xpath, "//img[@aria-label='First page'][@aria-disabled='true']");
    public static final Locator PREVIOUS_PAGE_BUTTON_DISABLED_LOCATOR = new Locator(LocatorType.Xpath, "//img[@aria-label='Previous page'][@aria-disabled='true']");
    public static final Locator NEXT_PAGE_BUTTON_DISABLED_LOCATOR = new Locator(LocatorType.Xpath, "//img[@aria-label='Next page'][@aria-disabled='true']");
    public static final Locator LAST_PAGE_BUTTON_DISABLED_LOCATOR = new Locator(LocatorType.Xpath, "//img[@aria-label='Last page'][@aria-disabled='true']");
}
