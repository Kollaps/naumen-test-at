package ru.abarabolenko.pages.celltablepage;

import ru.abarabolenko.testingframework.entities.contact.Contact;
import ru.abarabolenko.testingframework.managers.contact.ContactCreator;
import ru.abarabolenko.testingframework.testingtools.base.Locator;
import ru.abarabolenko.testingframework.testingtools.base.LocatorType;
import ru.abarabolenko.testingframework.testingtools.pageobjects.declaration.IWorkbench;

public class CellTablePageRow {

    public static Contact getContactFromRow(IWorkbench workbench, String index) {

        String format = "//table[@class='GNHGC04CIE GNHGC04CKJ']/tbody/tr[%s]/td[%s]/div";
        String categoryFormat = format.concat("/select");
        String firstName = workbench.getEditBox().getText(new Locator(LocatorType.Xpath, String.format(format, index, 2)));
        String lastName = workbench.getEditBox().getText(new Locator(LocatorType.Xpath, String.format(format, index, 3)));
        String category = workbench.getComboBox().getValue(new Locator(LocatorType.Xpath, String.format(categoryFormat, index, 4)));
        String address = workbench.getEditBox().getText(new Locator(LocatorType.Xpath, String.format(format, index, 5)));

        ContactCreator creator = new ContactCreator();
        creator.setFirstName(firstName);
        creator.setLastName(lastName);
        creator.setCategory(Contact.Category.valueOf(category));
        creator.setAddress(address);

        Contact contact = Contact.getEmptyContact();

        Contact.updateContact(contact, creator);

        return contact;
    }

}
