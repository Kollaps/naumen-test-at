package ru.abarabolenko.pages.cellListPage;

import ru.abarabolenko.testingframework.testingtools.base.Locator;
import ru.abarabolenko.testingframework.testingtools.base.LocatorType;
import ru.abarabolenko.testingframework.testingtools.pageobjects.declaration.IWorkbench;

public class CellListPage {

    public static final String url = "http://samples.gwtproject.org/samples/Showcase/Showcase.html#!CwCellList";

    public static final Locator CELL_LIST_LOCATOR = new Locator(LocatorType.Xpath, "//div[class='GNHGC04CGB']/div");

    public static final Locator FIRST_NAME_LOCATOR = new Locator(LocatorType.Xpath, "//td[@class='GNHGC04CFK'][text()=' First Name: ']/../td[2]/input");
    public static final Locator LAST_NAME_LOCATOR = new Locator(LocatorType.Xpath, "//td[@class='GNHGC04CFK'][text()=' Last Name: ']/../td[2]/input");
    public static final Locator CATEGORY_LOCATOR = new Locator(LocatorType.Xpath, "//td[@class='GNHGC04CFK'][text()=' Category: ']/../td[2]/select");
    public static final Locator BIRTHDAY_LOCATOR = new Locator(LocatorType.Xpath, "//td[@class='GNHGC04CFK'][text()=' Birthday: ']/../td[2]/input");
    public static final Locator ADDRESS_LOCATOR = new Locator(LocatorType.Xpath, "//td[@class='GNHGC04CFK'][text()=' Address: ']/../td[2]/textarea");

    public static final Locator CREATE_CONTRACT_BUTTON_LOCATOR = new Locator(LocatorType.Xpath, "//button[text()='Create Contact']");
    public static final Locator UPDATE_CONTRACT_BUTTON_LOCATOR = new Locator(LocatorType.Xpath, "//button[text()='Update Contact']");
    public static final Locator CREATE_50_CONTACTS_BUTTON_LOCATOR = new Locator(LocatorType.Xpath, "//button[text()='Generate 50 Contacts']");

    public static final Locator UPDATE_CONTRACT_BUTTON_DISABLED_LOCATOR = new Locator(LocatorType.Xpath, "//button[text()='Update Contact'][@disabled='']");

    public static final Locator CELL_LIST_PAGING_INFO_LOCATOR = new Locator(LocatorType.Xpath, "//div[@class='GNHGC04CJJ']/../div[2]");


    public static String getContactInfoFromList(IWorkbench workbench, int index) {

        String format = "//div[@class='GNHGC04CJJ']/div/div/div/div[%s]";
        Locator contactInfoLocator = new Locator(LocatorType.Xpath, String.format(format, index));

        String scriptScroller = "document.getElementsByClassName('GNHGC04CJJ')[0].scrollTop = document.getElementsByClassName('GNHGC04CJJ')[0].scrollHeight";

        while (true) {
            String listCountInfo = workbench.getTestingEngine().getElement().getText(CELL_LIST_PAGING_INFO_LOCATOR);
            String[] values = listCountInfo.split(" ");

            if(!values[2].equals(values[4]))
                workbench.getTestingEngine().getJavaScript().executeScript(scriptScroller);
            else
                break;
        }

        return workbench.getTestingEngine().getElement().getText(contactInfoLocator);
    }
}
